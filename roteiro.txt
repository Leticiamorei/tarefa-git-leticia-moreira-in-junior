git help: Mostra vários comandos de forma resumida

git help <comando>: Mostra detalhes de um comando específico

git config --global user.name “Seu Nome”
git config --global user.email “seuemaill@gmail.com”   (Usar o mesmo email do gitlab)
 
Para o repositório atual: --local;
Para o Usuário: --global;
Para o Computador: --system

Opcional:
(primeiro testa code -v)
git config --global core.editor 'code --wait' -> Vscode para editor padrão (precisa do code no PATH)


	Se tudo der certo:
	git config --global -e -> Abre o arquivo de configuração no vscode

-------------------------------------------------------


git init -> inicia repositório

git status -> pontua mudanças

o git é preguiçoso, tem que adicionar antes de commitar

git add arquivo/diretório -> adiciona um único arquivo para preparação e o deixa pronto para commitar
git add --all = git add .  -> adiciona todos arquivos para a preparação, para em seguida commitar-los

git commit -m “Primeiro commit” - > após adicionados(estando na área de preparação), salva as mudanças efetuadas no repositório local, constando a mensagem de commit , email, hora, data e o usuário

-------------------------------------------------------

git log -> mostra uma sequência de alterações em ordem cronológica com o detalhamento do commit, nome, email, data e hora 
git log arquivo -> mostra apenas uma lista de commits que afetam um arquivo específico

git reflog ->  registram quando o cume dos ramos, assim como, quais as outras referências que foram atualizadas no repositório local

-------------------------------------------------------

git show -> exibe informações expandidas de objetos git
git show <commit> -> mostra exatamente o que foi modificado no commit

-------------------------------------------------------

git diff -> Faz a comparação entre o que está na área de trabalho e commit mais recente.

git diff <commit1> <commit2> -> Faz a comparação entre dois commits. Mostra quais foram os arquivos alterados, novos e removidos. Além disso, mostra também quais foram as linhas alteradas

-------------------------------------------------------

git branch -> lista de todas as branches locais

git branch -r -> lista de todas as branches remotas

git branch -a -> lista de todas as branches remotas e locais

git branch -d <branch_name> -> deleta uma branche local

git branch -D <branch_name> -> força a remoção de um branch local. Esse comando é necessário, pois caso exista uma branch que ainda não foi feito o merge, git notifica e não permite apagar a branch somente com o
comando 'git branch -d'.

git branch -m <nome_novo> -> Essa alteração é muito simples de ser executada em branches locais, porém no caso de uma branch já estar no servidor e for o esperado renomear uma branch no servidor, é necessário
apagar e fazer a criação da branch com o nome correto.

git branch -m <nome_antigo> <nome_novo> -> Renomeia a branch especificada no <nome_antigo> para <nome_novo>. Caso esteja na branch que será feita a alteração, não é necessário fornecer o nome antigo, basta executar o comando git

-------------------------------------------------------

git checkout <branch_name> -> Caso o <nome> seja um branch:
- Muda o código para a branch selecionada. Alterações devem ser salvas (commited) antes de fazer a troca de branch.
Caso o <nome> seja um arquivo:
- Desfaz as alterações no arquivo, porém arquivo já deve ter sido adicionado nas mudanças.
Caso o <nome> seja uma tag:
- Muda o versionamento para onde a tag foi criada.
Caso o <nome> seja o hash de um commit:
- Muda o versionamento para o commit especificado.

git checkout -b <branch_name> -> Faz a criação de uma nova branch e já faz a troca do versionamento para a nova branch.

-------------------------------------------------------

git merge <branch_name> -> Mescla as mudanças presentes na <nome_da_branch> na branch corrente.


-------------------------------------------------------

git clone -> clona um repositório para a pasta desejada. Pode ser uma pasta local ou uma URL

git pull -> Atualiza repositório local com a última versão da origem da branch remota. Necessário commitar
as mudanças para executar essa ação.
Se for no servidor:
- Branch já deve estar rastreado no servidor. Assim, o git sabe que deve comparar a versão local com o servidor.
Se for no repositório local:
- Deve ter sido clonado de outra pasta para a pasta destino. Só assim, o repositório terá um 'pai' que poderá ser verificado para atualização de novas informações. Esse comando não funciona caso o versionamento seja somente local

git push -> Faz o envio das mudanças comitadas localmente para a origem da branch rastreada.

git push -u origin <nome_da_branch> -> Faz o envio da branch local para o servidor pela primeira vez. Caso a branch que está sendo enviada não exista no servidor, ela será criada. A partir desse momento, a branch local está configurada para ser rastreada com essa origem no servidor.
A partir desse primeiro comando, para versionar próximas mudanças basta que seja feito o comando 'git push' descrito acima.

-------------------------------------------------------

git remote -> Listar as conexões remotas que você tem com outros repositórios.

git remote -v -> Igual ao comando acima, mas inclui a URL de cada conexão. 

git remote add origin <url> -> Crie uma nova conexão com um repositório remoto. Depois de adicionar um remoto, você vai poder usar ＜name＞ como um atalho conveniente para ＜url＞ em outros comandos do Git.

git remote rename <old-name> <new-name> -> Renomeie uma conexão remota de ＜old-name＞ para ＜new-name＞.

--------------------------------------------------------

Documentação do git:
https://git-scm.com/doc

Playlist GIT:
https://www.youtube.com/playlist?list=PLxjCnO-f9JPT6Kuww_d2TJQK31Mz-cwR4
